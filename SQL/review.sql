CREATE DATABASE review_db DEFAULT CHARACTER SET utf8;

USE review_db;


CREATE TABLE `t_user` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL
  );



CREATE TABLE `t_clinic`(
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
    `address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
    `time` date DEFAULT NULL,
    `holiday` varchar(256)COLLATE utf8_unicode_ci DEFAULT NULL
    );
   ALTER TABLE t_clinic ADD holiday VARCHAR(256) COLLATE utf8_unicode_ci DEFAULT NULL;

CREATE TABLE `review_category`(
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
    );
    
    
    
CREATE TABLE `t_review`(
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `detail` text COLLATE utf8_unicode_ci,
    `create_date` datetime NOT NULL
    );
